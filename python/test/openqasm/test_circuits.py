import copy
import numpy as np
import pytest

from common import QubiC_OpenQasmTest, have_prerequisites

if not have_prerequisites:
   pytest.skip("prerequisites not met", allow_module_level=True)


class TestCIRCUITS(QubiC_OpenQasmTest):
    """Test conversion of qiskit circuits to QubiC"""

    externals = dict()

    options = {
        'barrier': 'none',
        'entangler' : 'cz',
        'h_impgate' : 'X90',
    }

    def test01_u3_operator(self):
        """Single qubit u3 operator"""

        prog, qubits = self._prep_program(1)

        for args in ((-0.28, 2.5, 3.88),):
            prog1 = copy.deepcopy(prog)
            prog1.gate(qubits[0], 'U', *args)

            self._verify2qubic(prog1, self.externals, self.options)

    def test02_single_operators(self):
        """Single qubit operators"""

        prog, qubits = self._prep_program(1)

        for gate in ('x', 'y', 'z', 'h'):
            prog1 = copy.deepcopy(prog)
            prog1.gate(qubits[0], gate)

            self._verify2qubic(prog1, self.externals, self.options, assemble=gate!='z')

      # special case to check both implementations of H
        alt = self.options.copy()
        alt['h_impgate'] = 'Y-90'
        prog1 = copy.deepcopy(prog)
        prog1.gate(qubits[0], gate)

        self._verify2qubic(prog1, self.externals, alt)

    def test03_single_rotations(self):
        """Single qubit rotations"""

        prog, qubits = self._prep_program(1)

        for gate in ('rx', 'ry', 'rz'):
            for phase in (np.pi, np.pi/2., np.pi/4., 1.234, 2800., -0.):
                prog1 = copy.deepcopy(prog)
                prog1.gate(qubits[0], gate, phase)

                self._verify2qubic(prog1, self.externals, self.options, assemble=gate!='rz')

