"""
Preliminary specification:
    qubits and gates:
        - quantum gates supported according to gate_map.GateMap
        - mapping between declared qubits (e.g. qubit q[n]) given by qubit_map.QubitMap
    classical variables:
        - all sized integers are cast to 32 bit native int types
        - all bit types are cast to an array of integers
        - all floats are cast to native amp types
        - all angles are cast to native phase types
    classical flow:
        - if/else are converted to branch_var statements
        - for and while loops are supported
            - break, continue not supported
"""

NATIVE_INT_SIZE = 32

from openqasm3.visitor import QASMVisitor, QASMNode
import openpulse.ast as ast
from distproc.openqasm.qubit_map import QubitMap, QASMQubitMap, OpenPulseQubitMap
from distproc.openqasm.gate_map import GateMap, QASMGateMap, OpenPulseGateMap
import numpy as np
import operator
import warnings
from attrs import define

__all__ = ['QASMQubiCVisitor',
           'OpenPulseQubiCVisitor',
          ]

time_units = {
    ast.TimeUnit.ns: 1e-9,
    ast.TimeUnit.us: 1e-6,
    ast.TimeUnit.ms: 1e-3,
    ast.TimeUnit.s:  1e-0,
}

constants = {
    'pi' : np.pi,
}

op_mapping = {
    '==': 'eq',
    '>':  'ge',
    '<':  'le',
    '+':  'add',
    '-':  'sub',
}

unary_ops = {
    '+' : operator.pos,
    '-' : operator.neg,
}

binary_ops = {
    '+' : operator.add,
    '-' : operator.sub,
    '*' : operator.mul,
    '/' : operator.truediv,
}

def eval_expression(node: ast.Expression):
    if isinstance(node, ast.FloatLiteral):
        return float(node.value)
    elif isinstance(node, ast.IntegerLiteral):
        return int(node.value)
    elif isinstance(node, ast.DurationLiteral):
        return node.value * time_units[node.unit]
    elif isinstance(node, ast.Identifier):
        try:
            return constants[node.name]
        except KeyError:
            return node.name
    elif isinstance(node, ast.UnaryExpression):
        return unary_ops[node.op.name](eval_expression(node.expression))
    elif isinstance(node, ast.BinaryExpression):
        lhs = eval_expression(node.lhs)
        rhs = eval_expression(node.rhs)
        return binary_ops[node.op.name](lhs, rhs)
    elif isinstance(node, ast.IndexExpression):
        assert len(node.index) == 1 and "only single indexing is supported"
        index = node.index[0]
        return node.collection.name + ('_%d' % index.value)
    else:
        raise NotImplementedError(str(node))

def eval_variable(node: ast.QASMNode, vars: dict):
    if isinstance(node, ast.IndexedIdentifier):
        name = node.name.name
        assert len(node.indices) == 1 and \
               len(node.indices[0]) == 1 and "only single indexing is supported"
        index = node.indices[0][0].value
        name += ('_%d' % index)
    elif isinstance(node, ast.Identifier):
        name = node.name
        assert name in vars
    else:
        raise NotImplementedError(str(node))

    return name


@define
class _VariableContainer:
    var_names: list
    type: str = 'int'


class QASMQubiCVisitor(QASMVisitor):

    def __init__(self, qubit_map: QubitMap=QASMQubitMap(), gate_map: GateMap=QASMGateMap()):
        self.qubit_map = qubit_map
        self.gate_map = gate_map
        self.program = []
        self._cur_block = self.program #pointer to current instr list

        self.qubits = {}
        self.vars = {}

        self._vardecls = dict()
        self._measurements = dict()

        super().__init__()

    def _bind_phase(self, qubit):
        varname = "_bind_phase"+qubit
        if not varname in self._vardecls:
            vardecl = {'name': 'declare', 'var': varname, 'dtype': 'phase', 'scope': [qubit]}
            self._vardecls[varname] = vardecl
            self.program = [vardecl,
                            {'name': 'bind_phase', 'freq': f'{qubit}.freq', 'var': varname}] +\
                            self.program

    def _declare_unscoped_var(self, varname: str, vartype: str=None):
        vardecl = {'name': 'declare', 'var': varname, 'scope': None}
        self._cur_block.append(vardecl)
        self._vardecls[varname] = vardecl
        if vartype is not None:
            self.vars[varname] = _VariableContainer([varname], vartype)

    def _update_scope(self, varname: str, scope: set):
        cur = self._vardecls[varname]['scope']
        if cur is None:
            self._vardecls[varname]['scope'] = scope
        else:
            self._vardecls[varname]['scope'].update(cur)

    def _get_hardware_qubits(self, qasm_qubits: list):
        hw_qubits = list()
        for qid in qasm_qubits:
            if isinstance(qid, ast.Identifier):          # single qubit
                assert self.qubits[qid.name] is None
                hw_qubits.append(self.qubit_map.get_hardware_qubit(qid.name))
            elif isinstance(qid, ast.IndexedIdentifier): # index into qubit register
                hw_qubits.append(self.qubit_map.get_hardware_qubit(qid.name.name,
                                                                   qid.indices[0][0].value))
            else:
                raise RuntimeError("cannot identify hardware qubit for", str(qid))

        return hw_qubits

    def visit(self, node: QASMNode, context=None, qubits=None):
        if qubits is not None:
            # simple linear qubit indexing
            self.qubits = {"$%d" % q : None for q in qubits}

        return super().visit(node, context)

    def visit_BinaryExpression(self, node: QASMNode, context=None):
        op = node.op.name
        lhs = eval_expression(node.lhs)
        rhs = eval_expression(node.rhs)

        if not isinstance(rhs, str):
            # rhs must be a variable; if it is a value, then invert the logic,
            # which requires rhs to be an int and lhs to be of bit type until
            # the IR is extended to support other types
            assert self.vars[lhs].type in ('bit', 'int')
            assert isinstance(rhs, int)
            if op == "==":
                t = lhs; lhs = rhs; rhs = t
            else:
                assert not "operation inversion not yet implemented"

        if context != "branch":
            self._cur_block.append({'name': 'alu', 'op': op_mapping[op],
                                    'lhs': lhs, 'rhs': rhs, 'out': rhs})

        return lhs, op_mapping[op], rhs

    def visit_BranchingStatement(self, node: QASMNode, context=None):
        cond_lhs, op, cond_rhs = self.visit(node.condition, "branch")

        true_block, false_block = list(), list()
        for qasm_block, qubic_block in ((node.if_block, true_block),
                                        (node.else_block, false_block)):
            self._cur_block = qubic_block
            for instr in qasm_block:
                self.visit(instr, context)

        self._cur_block = self.program

        scope = set()
        for block in (true_block, false_block):
            for instr in block:
                try:
                    [scope.add(q) for q in instr['qubit']]
                except KeyError:
                    pass

                if instr['name'] == 'virtual_z':
                    # conditional Z can not be virtual, so bind it to a variable
                    [self._bind_phase(q) for q in instr['qubit']]

        if scope:
            self._update_scope(cond_rhs, scope)

        instr = {'cond_lhs': cond_lhs, 'alu_cond': op,
                 'scope': list(self._vardecls[cond_rhs]['scope']),
                 'true': true_block,
                 'false': false_block}

        if cond_rhs in self._measurements:
            # condition is on a measurement result; the OpenQASM variable does
            # not matter per se: the measurement output is directly identified
            instr['name'] = 'branch_fproc'
            qubits = self._measurements[cond_rhs]
            assert len(qubits) == 1
            instr['func_id'] = qubits[0]+'.meas'
        else:
            # condition is on a variable that received its value through some
            # other means than a measurement
            instr['name'] = 'branch_var'
            instr['cond_rhs'] = cond_rhs

        self._cur_block.append(instr)

    def visit_ClassicalDeclaration(self, node: QASMNode, context=None):
        if isinstance(node.type, ast.BitType):
            if node.type.size is None or node.type.size.value is None:
                self._declare_unscoped_var(node.identifier.name, 'int')
            else:
                for i in range(node.type.size.value):
                    self._declare_unscoped_var(f'{node.identifier.name}_{i}', 'int')

            return

        elif isinstance(node.type, ast.IntType):
            if node.type.size is not None and node.type.size != NATIVE_INT_SIZE:
                warnings.warn(f'casting integer of size {node.type.size} to {NATIVE_INT_SIZE}')
            self._declare_unscoped_var(node.identifier.name, 'int')

            return

        elif isinstance(node.type, ast.ArrayType):
            # unroll the array, declaring single variables for each element
            assert len(node.type.dimensions) == 1 and "only 1-dim arrays supported"
            for i in range(node.type.dimensions[0].value):
                self._declare_unscoped_var(f'{node.identifier.name}_{i}', 'int')

            return

        assert not node

    def visit_FloatLiteral(self, node: QASMNode, context=None):
        return eval_expression(node)

    def visit_Identifier(self, node: QASMNode, context=None):
        return self.vars[node.name].var_names[0]

    def visit_IndexExpression(self, node: QASMNode, context=None):
        assert isinstance(node.index[0], ast.IntegerLiteral)
        assert len(node.index) == 1
        return self.vars[node.collection.name].var_names[node.index[0]]

    def visit_IntegerLiteral(self, node: QASMNode, context=None):
        return eval_expression(node)

    def visit_QubitDeclaration(self, node: QASMNode, context=None):
        if node.size is not None and node.size.value != 1:
            self.qubits[node.qubit.name] = node.size.value
        else:
            self.qubits[node.qubit.name] = None

    def visit_QuantumGate(self, node: QASMNode, context=None):
        gatename = node.name.name

        params = list(eval_expression(a) for a in node.arguments)
        qubits = self._get_hardware_qubits(node.qubits)

        self._cur_block.extend(
            self.gate_map.get_qubic_gateinstr(gatename, qubits, params, self.options)
        )

    def visit_QuantumMeasurement(self, node: ast.QuantumMeasurement, context=None):
        qubits = self._get_hardware_qubits([node.qubit])
        self._cur_block.append({'name': 'read', 'qubit': qubits[0]})

    def visit_QuantumMeasurementStatement(self, node: ast.QuantumMeasurement, context=None):
        qubits = self._get_hardware_qubits([node.measure.qubit])
        output = eval_variable(node.target, self.vars)
        self._measurements[output] = qubits
        self._update_scope(output, set(qubits))
        return self.visit(node.measure)

    def visit_QuantumReset(self, node: QASMNode, context=None):
        hw_qubits = self._get_hardware_qubits(node.qubits)

        for qubit in hw_qubits:
            self._cur_block.extend([
                {'name': 'read', 'qubit': qubit},
                {'name': 'branch_fproc', 'cond_lhs': 1, 'alu_cond': 'eq', 'func_id': f'{qubit}.meas', 'scope': qubit,
                    'true': [
                        {'name': 'X90', 'qubit': qubit},
                        {'name': 'X90', 'qubit': qubit}],
                    'false': []}])


class OpenPulseQubiCVisitor(QASMQubiCVisitor):
    def __init__(self, externals: dict, options: dict={}, *args, **kwds):
        super().__init__(qubit_map=OpenPulseQubitMap(), gate_map=OpenPulseGateMap(), *args, **kwds)

        # classical functions, linkable with this program
        self.externals = externals

        # options to drive QubiC codegen
        self.options = options

        self.qchip = None

    def visit(self, node: QASMNode, context=None, qubits=None, qchip=None):
        if qchip is not None:
            self.qchip = qchip

        result = super().visit(node, context, qubits=qubits)

        if self.qchip and self.gate_map:
            # custom gates collected: update qchip's gate_map
            self.qchip.gates.update(self.gate_map.custom_gates)

        return result

    def visit_CalibrationDefinition(self, node: ast.CalibrationDefinition, context=None):
        qubits = self._get_hardware_qubits(node.qubits)
        name = qubits+node.name.name.upper()

        pulses = list()
        for stmt in node.body:
            expr = stmt.expression
            if isinstance(expr, ast.FunctionCall):
                if expr.name.name == 'play':
                    args = expr.arguments
                    dest = args[0].name
                    env  = args[1].name.name
                    args = tuple(eval_expression(a) for a in args[1].arguments)
                    pulse_kwds = self.externals[env](*args)

                    pulses.append(qbqc.GatePulse(dest=dest, freq=42, t0=2, **pulse_kwds))
            # phase, freq, dest, amp, t0=None, twidth=None, env=None, gate=None, chip=None
                else:
                    raise NotImplementedError(expr.name)
            else:
                raise NotImplementedError(str(stmt.expression))

        gate = qbqc.Gate(pulses, self.qchip, name)
        self.gate_map.add_custom_gate(name, gate)

        return node

    def visit_CalibrationGrammarDeclaration(self, node: ast.CalibrationGrammarDeclaration, context=None):
        assert(node.name == 'openpulse')

    def visit_CalibrationStatement(self, node: ast.CalibrationStatement, context=None):
        for node in node.body:
            self.visit(node, context)
        return node

    def visit_ClassicalDeclaration(self, node: QASMNode, context=None):
        if isinstance(node.type, ast.PortType):
            # TODO: at least verify validity
            return

        elif isinstance(node.type, ast.FrameType):
            # TODO: capture carrier frequency
            return

        elif isinstance(node, ast.ExternDeclaration):
            self.vars[node.name.name] = node
            return

        return super().visit_ClassicalDeclaration(node, context)

    def visit_ForInLoop(self, node: ast.ForInLoop, context=None):
        var  = node.identifier.name
        decl = node.set_declaration

        start, end = decl.start.value, decl.end.value
        step = 1
        if node.set_declaration.step:
            step = node.set_declaration.step

        for node in node.block:
            self.visit(node, context)

        return

    def visit_Identifier(self, node: ast.Identifier, context=None):
        for c in (self.vars, self.qubits, self.externals):
            try:
                return c[node.name]
            except KeyError:
                pass

        raise RuntimeError("undeclared identifier: %s" % node.name)

